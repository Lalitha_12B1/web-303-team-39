import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PAGESComponent } from './pages.component';

describe('PAGESComponent', () => {
  let component: PAGESComponent;
  let fixture: ComponentFixture<PAGESComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PAGESComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PAGESComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
