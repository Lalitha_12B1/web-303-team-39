import { ComponentFixture, TestBed } from '@angular/core/testing';

import { COMPONENTSComponent } from './components.component';

describe('COMPONENTSComponent', () => {
  let component: COMPONENTSComponent;
  let fixture: ComponentFixture<COMPONENTSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ COMPONENTSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(COMPONENTSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
