import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../SERVICE/api.service';

@Component({
  selector: 'app-home',
  templateUrl:'./home.Component.html',
  styleUrls: ['./home.Component.css']
})
export class HomeComponent implements OnInit {
  items: any = [];
  constructor(private api: ApiService) { }
   

  ngOnInit(): void {
    this.getproducts()
  }
  getproducts(): void{
    this.api.getJson().subscribe(resp=>{
      this.items = resp
    })
  }

}
